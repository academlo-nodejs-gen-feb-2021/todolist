const {
    createTask
} = require("../services/task.service");

const render = async (req, res, next) => {
    let { id, firstname, lastname } = req.user;

    try {
        let username = `${firstname} ${lastname}`;

        let tasks = await categoriesByUser(id);
        return res.render("pages/tasks", {
            title: "Tareas",
            username,
            tasks
        });
    } catch (error) {
        next(error);
    }
};

const _delete = async (req, res, next) => {
    try {
        //Obtenemos el parametro id
        let categoryId = req.params.id;
        await deleteCategory(categoryId);
        res.redirect("/tareas");
    } catch (error) {
        next(error);
    }
};

const create = async (req, res, next) => {
    try {
        //Obtenemos el parametro id
        let { id: userId } = req.user;

        let taskObj = { title, description, due_date, user_id: userId, category_id, status_id} = req.body;
        await createTask(taskObj);
        res.redirect("/tareas");
    } catch (error) {
        next(error);
    }
}

const renderEdit = async (req, res, next) => {
    let { firstname, lastname } = req.user;
    let { id:categoryId } = req.params;

    try {
        let username = `${firstname} ${lastname}`;
        let category = await categoryById(categoryId);
        return res.render("pages/edit-category", {
            title: "Editar categorias",
            username,
            id: categoryId,
            name: category.name
        });
    } catch (error) {
        next(error);
    }
};

const update = async (req, res, next) => {
    try {
        //Obtenemos el parametro id
        let { id:categoryId } = req.params;
        let { name } = req.body;
        await updateCategory({name, categoryId});
        res.redirect("/categorias");
    } catch (error) {
        next(error);
    }
}

module.exports = {
    render,
    create,
    update,
    renderEdit,
    _delete,
};

const passport = require("passport");
const { newUser } = require("../services/auth.service");
const {sendMail, emailOptions} = require("../config/nodemailer");
const ejs = require("ejs");
const path = require("path");

const renderLogin = (req, res) => {
    res.render("pages/login", { title: "Iniciar sesión" });
};

const renderRegister = (req, res) => {
    res.render("pages/register", { title: "Registro" });
};

const register = async (req, res, next) => {
    let { firstname, lastname, email, password } = req.body;

    try {
        await newUser({ firstname, lastname, email, password });
        res.redirect("/registro");
    } catch (error) {
        next(error);
    }
};

const logout = (req, res) => {
    req.logout(); //Quitamos la sesión activa del usuario
    return res.redirect("/login"); //Redireccionamos a inicio
};

const localAuthStrategy = passport.authenticate("local", {
    successRedirect: "/categorias",
    failureRedirect: "/login",
});

const gAuthStrategy = passport.authenticate("google", {
    session: true,
    scope: ["email", "profile"],
});

const fbAuthStrategy = passport.authenticate("facebook", {
    session: true,
    scope: ["email", "public_profile"],
});

const gCallback = passport.authenticate('google', {
    successRedirect: '/categorias',
    failureRedirect: '/login'
});

const fbCallback = passport.authenticate('facebook', {
    successRedirect: '/categorias',
    failureRedirect: '/login'
});

const resetPassword = async (req, res, next) => {
    try{
        //Obtenemos el email del usuario que quiere restablecer su contraseña
        const {email} = req.body;
        //Enviar el correo electronico con las instrucciones para el restablecimiento
        emailOptions.to = email; //Va dirigido hacía el cliente
        emailOptions.subject = "Restablecimiento de contraseña";
        const template = await ejs.renderFile(path.join(__dirname, "..", "views", "email-templates", "reset-password.ejs"), {title: "Restablecer tu contraseña"});
        emailOptions.html = template;
        await sendMail(emailOptions);
        res.send("Se ha enviado el correo satisfactoriamente");
    }catch(error){
        next(error);
    }
}

module.exports = {
    renderLogin,
    renderRegister,
    register,
    logout,
    localAuthStrategy,
    gAuthStrategy,
    fbAuthStrategy,
    gCallback,
    fbCallback,
    resetPassword
};

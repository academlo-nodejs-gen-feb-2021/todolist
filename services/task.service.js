const { Task } = require("../models");

const tasksByUser = async (userId) => {
    try {
        let tasks = await Task.findAll({
            where: { created_by: userId },
            raw: true,
        });
        return tasks;
    } catch (error) {
        throw new Error(error);
    }
};

const tasksById = async (taskId) => {
    try {
        let task = await Task.findByPk(taskId);
        return task;
    } catch (error) {
        throw new Error(error);
    }
};

const createTask = async (taskObj) => {


    let newTask = {
        title: taskObj.title,
        description: taskObj.description,
        due_date: taskObj.dueDate,
        user_id: taskObj.userId,
        category_id: taskObj.categoryId,
        status_id: taskObj.statusId
    };

    try {
        let task = await Task.create(newTask);
        return task;
    } catch (error) {
        throw new Error(error);
    }
};

const updateTask = async (taskObj, taskId) => {
    try {
        let newTask = {
            title: taskObj.title,
            description: taskObj.description,
            due_date: taskObj.dueDate,
            user_id: taskObj.userId,
            category_id: taskObj.categoryId,
            status_id: taskObj.statusId
        };

        let task = await Task.update(
            newTask,
            { where: { id: taskId } }
        );
        return task;
    } catch (error) {
        throw new Error(error);
    }
};

const deleteTask = async (taskId) => {
    try {
        let results = await Task.destroy({
            where: { id: taskId },
        });
        return results;
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = {
    tasksById,
    tasksByUser,
    createTask,
    updateTask,
    deleteTask,
};

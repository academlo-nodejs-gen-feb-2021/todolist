const {Router} = require('express');
// const  = require('../controllers/category.controller');
const protectRoute = require('../middlewares/protect-routes');

const router = Router();

router.get('/tareas', protectRoute, catCtrl.render);

router.post('/tareas', protectRoute, catCtrl.create);

router.get('/tareas/borrar/:id', protectRoute, catCtrl._delete);

router.get('/tareas/editar/:id', protectRoute, catCtrl.renderEdit);
router.post('/tareas/editar/:id', protectRoute, catCtrl.update);


module.exports = router;